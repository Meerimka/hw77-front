import axios from '../../axios-messages';


export const MESSAGES_SUCCESS  = 'MESSAGES_SUCCESS';

export const getMessagesSuccess = (messages) => ({type: MESSAGES_SUCCESS,messages});


export const getMessages = ()=>{
    return dispatch =>{
        return axios.get('/messages').then(
            response =>{
                console.log(response.data);
                return dispatch(getMessagesSuccess(response.data))
            }
        )
    }
};

export const sendMessage = (data) =>{
    return (dispatch) => {
        return axios.post('/messages', data)
            .then(
            ()=>dispatch(getMessages())
        )
    }
};


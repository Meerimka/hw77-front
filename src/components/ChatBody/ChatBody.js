import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import '../../App.css';
import {sendMessage} from "../../store/action/messages";
import {connect} from "react-redux";

class ChatBody extends Component {

    state = {
        message: '',
        author: '',
        image: null
    };

    changeHandler = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    };

    fileChangeHadler = event =>{
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };


    sendHandler = event => {

        event.preventDefault();
        const formData = new FormData();

        Object.keys(this.state).forEach(key =>{
            formData.append(key, this.state[key]);
        });

        this.props.sendMessage(formData)
    };

    render() {
        return (
            <Form className="Form-box" inline>
                <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="Author" className="mr-sm-2">Your name </Label>
                    <Input type="text" value={this.state.author} name="author" id="Author" onChange={this.changeHandler} placeholder="enter your name" />
                </FormGroup>
                <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="Message" className="mr-sm-2">Text</Label>
                    <Col sm={10}>
                        <Input type="text" value={this.state.message} name="message" id="Message" onChange={this.changeHandler} placeholder="type a text here" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="image">Image</Label>
                    <Col sm={10}>
                        <Input
                            type="file"
                            name="image" id="image"
                            onChange={this.fileChangeHadler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{offset:2, size: 10}}>
                        <Button onClick={this.sendHandler}>Send</Button>
                    </Col>
                </FormGroup>
            </Form>
        )
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        sendMessage: (data)  => dispatch(sendMessage(data)),
    }};


export default connect(null,mapDispatchToProps)(ChatBody);
import React from 'react';
import {apiURL} from "../../constants";


const styles = {
    width: '100px',
    height: '100px',
    marginRight: '10px'
};


const MessageThumbnail = (props) => {
    if(props.image) {
        return <img src={apiURL + '/uploads/' + props.image} style={styles} className="img-thumbnail" alt="Message image"/>
    } else {
        return null
    }
};


export default MessageThumbnail;
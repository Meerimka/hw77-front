import React, { Component } from 'react';
import './App.css';
import ChatBody from "./components/ChatBody/ChatBody";
import Messages from "./components/Messages/Messages";

class App extends Component {
  render() {
    return (
        <div className="App">
            <div className="Message-container">
                <Messages/>
            </div>

            <ChatBody/>
        </div>

    );
  }
}

export default App;

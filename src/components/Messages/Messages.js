import React, {Component} from 'react';
import {getMessages} from "../../store/action/messages";
import {connect} from "react-redux";
import MessageThumbnail from "../MessageThumbnail/MessageThumbnail";


const styles = {
    padding: "10px",
    border: "1px solid gray",
    width: "400px",
    height: "auto",
    borderRadius: "3px",
    marginBottom: "5px"
};


class Messages extends Component {

    componentDidMount() {
        this.props.getMessages()
    }

    render() {
        return (
            <div>{this.props.messages.map((message, id) => {
                return (
                    <div style={styles}  key={id}>
                        <span>{message.author}:{' '}</span>

                        <span>{' '}{message.message}{' '}</span><br/>
                        <span>{' '}{message.date}{' '}</span>
                        <MessageThumbnail image={message.image}/>
                    </div>
                )
            })
            }
            </div>
        )
    }
}
const mapStateToProps= state =>({
    messages:state.message.messages
});

const mapDispatchToProps = dispatch =>({
    getMessages: () => dispatch(getMessages())
});

export default connect(mapStateToProps,mapDispatchToProps)(Messages);